for(var i = 0; i < $('.header .carousel').find('.carousel-item').length; i++) {
	$('.header .carousel .carousel-indicators').append('<li data-target="#carousel-1" data-slide-to="' + i+ '"></li>');
}

$('.header .carousel').find('li:nth-child(1)').addClass('active');

$('#news_carousel').owlCarousel({
    loop:true,
    dots: false,
    autoplay:true,
	autoplayTimeout:4000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        768: {
        	items: 3
        },
        1200:{
            items:4
        }
    }
})

$('#students_carousel').owlCarousel({
    loop:true,
    dots: false,
    autoplay:true,
	autoplayTimeout:4000,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:2
        },
        768: {
        	items: 3
        },
        1200:{
            items:4
        }
    }
})

$('#partners_carousel').owlCarousel({
    loop:true,
    dots: false,
    autoplay:true,
	autoplayTimeout:4000,
    responsive:{
        0:{
            items:3
        },
        600:{
            items:2
        },
        768: {
        	items: 4
        }
    }
})

var owl1 = $('#news_carousel');
owl1.owlCarousel();
// Go to the next item
$('.carousel_2_next').click(function() {
    owl1.trigger('next.owl.carousel');
})
// Go to the previous item
$('.carousel_2_prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl1.trigger('prev.owl.carousel', [300]);
})

var owl2 = $('#students_carousel');
owl2.owlCarousel();
// Go to the next item
$('.carousel_3_next').click(function() {
    owl2.trigger('next.owl.carousel');
})
// Go to the previous item
$('.carousel_3_prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl2.trigger('prev.owl.carousel', [300]);
})

var owl3 = $('#partners_carousel');
owl2.owlCarousel();
// Go to the next item
$('.carousel_4_next').click(function() {
    owl3.trigger('next.owl.carousel');
})
// Go to the previous item
$('.carousel_4_prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl3.trigger('prev.owl.carousel', [300]);
})

// ;


$('.hamburger_button').on('click', function() {
	$('.hamburger').slideToggle(150);
});

$('.news_content .inner .owl-carousel .item').on('click', function() {
    $('.news_popup').fadeIn(200).css('display', 'flex');
    $('#news_popup_carousel').owlCarousel({
        loop:true,
        dots: false,
        autoplay:true,
        autoplayTimeout:5000,
        margin:10,
        items: 1
    })
    $('.news_popup .owl-item').find('.item .text').each(function() {
    var _text = $(this).text();
    var _new_text = _text.substring(0, 570);
    $(this).text(_new_text + '...');
    });
});
// Go to the next item
$('.carousel_news_next').click(function() {
    $('#news_popup_carousel').trigger('next.owl.carousel');
})
// Go to the previous item
$('.carousel_news_prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    $('#news_popup_carousel').trigger('prev.owl.carousel', [300]);
})

// ;

$('#news_popup_carousel').on('changed.owl.carousel', function(e) {
    $('#news_popup_carousel').trigger('stop.owl.autoplay');
    $('#news_popup_carousel').trigger('play.owl.autoplay');
});

$('.news_content .news_popup .close_popup').on('click', function() {
    $('.news_popup').fadeOut(200).css('display', 'flex');
    var news_carousel_popup = $('#news_popup_carousel');
    news_carousel_popup.trigger('destroy.owl.carousel');
    news_carousel_popup.addClass('off');
});



$('.students .inner .owl-carousel .item').on('click', function() {
    $('.students_popup').fadeIn(200).css('display', 'flex');
    $('#students_popup_carousel').owlCarousel({
        loop:true,
        dots: false,
        autoplay:true,
        autoplayTimeout:5000,
        margin:10,
        items: 1
    })
    $('.students_popup .owl-item').find('.item .text').each(function() {
    var _text = $(this).text();
    var _new_text = _text.substring(0, 470);
    $(this).text(_new_text + '...');
    });
});

// Go to the next item
$('.carousel_students_next').click(function() {
    $('#students_popup_carousel').trigger('next.owl.carousel');
})
// Go to the previous item
$('.carousel_students_prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    $('#students_popup_carousel').trigger('prev.owl.carousel', [300]);
})

// ;

$('#students_popup_carousel').on('changed.owl.carousel', function(e) {
    $('#students_popup_carousel').trigger('stop.owl.autoplay');
    $('#students_popup_carousel').trigger('play.owl.autoplay');
});


$('.students .students_popup .close_popup').on('click', function() {
    $('.students_popup').fadeOut(200).css('display', 'flex');
    var news_carousel_popup = $('#students_popup_carousel');
    news_carousel_popup.trigger('destroy.owl.carousel');
    news_carousel_popup.addClass('off');
});



